# SUPER UFO 🛸

[Minetest mod] Add a UFO that allows the player to have a fly house. 

## Screenshot:

![screenshot]

Outras Screnshots: [screenshot2] | [screenshot3]

## Depends:
 * **default** (mod of standard game)
 * **[intllib]** (optional) ← Internationalization library for Minetest. 
 * **[minertrade]** (optional) ← Opens the carrier trunk of UFO that is interconnected with the Public ATM and the Personal Strongbox.
 
## Licence: 
 * [GNU AGPL]

## Repositories:
 * [super-ufo] → This version is a fork of [ufos].
 * [ufos] ← Mod original of Zeg9

## Developers:
 * [Zeg9] ← for code
 * **Melkor** ← for model and texture
 * [Lunovox Heavenfinder] ← for code and portuguese translate 

## keyboard shortcuts:
 * ````S Key + Run Key```` ← Sound effect of '[five tones]' of film in 1977, '[close encounters of the third kind]'.
 * ````S Key + Sneak Key```` ← Emergency Ejection to out of the UFO.

## Languages:
 * en ← [English] (default, no need extra mod).
 * pt ← [Portuguese] per command ````/set language pt````. Need mod '**[intllib]**' to work.

Contact a developer of this mod to add a translate to your language!

[screenshot]:https://gitlab.com/lunovox/super-ufo/-/raw/master/screenshot.png
[screenshot2]:https://gitlab.com/lunovox/super-ufo/-/raw/master/screenshot2.png
[screenshot3]:https://gitlab.com/lunovox/super-ufo/-/raw/master/screenshot3.png
[GNU AGPL]:https://gitlab.com/lunovox/super-ufo/-/raw/master/LICENSE
[intllib]:https://github.com/minetest-mods/intllib
[minertrade]:https://github.com/Lunovox/minertrade
[ufos]:https://github.com/Zeg9/minetest-ufos/
[super-ufo]:https://gitlab.com/lunovox/super-ufo
[Zeg9]:https://github.com/Zeg9
[Lunovox Heavenfinder]:https://libreplanet.org/wiki/User:Lunovox
[five tones]:https://gitlab.com/lunovox/super-ufo/-/raw/master/sounds/sfx_five_tones.ogg
[close encounters of the third kind]:https://duckduckgo.com/?q=close+encounters+of+the+third+kind&t=lm&ia=web
[English]:https://gitlab.com/lunovox/super-ufo/-/raw/master/sounds/welcome_en.ogg
[Portuguese]:https://gitlab.com/lunovox/super-ufo/-/raw/master/sounds/welcome_pt.ogg
