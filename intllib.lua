local S
local SN

--[[
local S = minetest.get_translator('testmod')
minetest.register_craftitem('testmod:test', {
    description = S('I am a test object'),
    inventory_image = 'default_stick.png^[brighten'
})
--]]

if minetest.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		S, SN = intllib.make_gettext_pair()
   if type(NS)=="function" then
		   modUFO.translate = NS
   else
    modUFO.translate = S
   end
	else
		-- Old method using text files.
		modUFO.translate = intllib.Getter()
	end
--[[ --Tive que desaivar esse tradutor 5.1.x porque ele está bugando a codificaça de caracteres que gera uma má seleção de áudios traduzidos no ufo.
elseif minetest.get_translator ~= nil and minetest.get_current_modname ~= nil and minetest.get_modpath(minetest.get_current_modname()) then
	modUFO.translate = minetest.get_translator(minetest.get_current_modname())
--]]
else
	modUFO.translate = function(s) return s end
end
